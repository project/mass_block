<?php

/**
 * @file mass_block.admin.inc
 * Functions that are only called on the admin pages.
 */

/**
 * Module settings form.
 */
function mass_block_settings() {

  $form['mass_block_info'] = array(
    '#value' => t('<p>The mass_block module allows you to block a list of users.</p>')
    );

  return system_settings_form($form);
}

/**
 * Function to mass block user.
 */
function mass_block_submit($form, &$form_state) {

	// we prepare return arrays
	$added = array();
	$invalid = array();
	$added2 = array();
	$invalid2 = array();
	// we get values
  	$emails = preg_split("/[\s,]+/", $form_state['values']['emails']);
  	$names = preg_split("/[\s,]+/", $form_state['values']['names']);

  	// we parse the emails list
  	foreach ($emails as $email) {
	    $email = trim($email);
	    $email=(string) $email;
	    if ($email!="") {
		    if (valid_email_address($email)) {
	  			if ($account = user_load(array('mail' => $email))){
					if ($account !== FALSE ) {
						user_block_user_action(&$account);
					}
					$added[] =$email;
				}else{
					$invalid[] =$email;
				}
		    }
	    }
	}
	  	
	// we parse the users list
  	foreach ($names as $name) {
	    $name = trim($name);
	    $name=(string) $name;
	    if ($name!=""){
		    if ($account = user_load(array('name' => $name))){
				if ($account !== FALSE && !isset($account->roles[$rid])) {
					user_block_user_action(&$account);
				}
				$added2[] =$name;
			}else{
				$invalid2[] =$name;
			}
	    }
	}

	if (count($added)>0) {
		$added = implode(", ", $added);
		drupal_set_message(t('The following addresses were updated: %added .', array('%added' => $added)));
		}
	else {
		drupal_set_message(t('No addresses were updated.'));
	}
	if (count($invalid)>0) {
		$invalid = implode(", ", $invalid);
		drupal_set_message(t('The following addresses were invalid: %invalid .', array('%invalid' => $invalid)), 'error');
	}
	if (count($added2)>0) {
		$added2 = implode(", ", $added2);
		drupal_set_message(t('The following users were updated: %added2 .', array('%added2' => $added2)));
		}
	else {
		drupal_set_message(t('No users were updated.'));
	}
	if (count($invalid2)>0) {
		$invalid2 = implode(", ", $invalid2);
		drupal_set_message(t('The following users were invalid: %invalid2 .', array('%invalid2' => $invalid2)), 'error');
	}
	
	
}

/**
 * Function to display mass adding form
 */
function mass_block() {

  global $language;

  $form['emails'] = array(
    '#type' => 'textarea',
    '#title' => t('Email addresses'),
    '#cols' => 60,
    '#rows' => 5,
    '#description' => t('Email addresses must be separated by comma, space or newline.'),
  );
  $form['names'] = array(
    '#type' => 'textarea',
    '#title' => t('User names'),
    '#cols' => 60,
    '#rows' => 5,
    '#description' => t('User names must be separated by comma, space or newline.'),
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Block'),
  );
  return $form;
}